/*
| Maps file types to mimes and encodings.
*/
def.abstract = true;

/*
| Coding mappings for file types.
*/
const codings =
{
	'bcmap' : 'binary', // something something pdfjs
	'css'   : 'utf-8',  // cascading style sheet
	'eot'   : 'binary', // some font
	'gif'   : 'binary', // image
	'html'  : 'utf-8',  // hypertext
	'map'   : 'utf-8',  // map
	'ico'   : 'binary', // icon
	'jpeg'  : 'binary', // image
	'jpg'   : 'binary', // image
	'cjs'   : 'utf-8',  // javascript
	'js'    : 'utf-8',  // javascript
	'otf'   : 'binary', // some font
	'pdf'   : 'binary', // pdf file
	'pfb'   : 'binary', // font file
	'png'   : 'binary', // image
	'svg'   : 'utf-8',  // some font or image
	'ttf'   : 'binary', // some font
	'woff'  : 'binary', // some font
	'woff2' : 'binary', // some font
};

/*
| mime mappings for file types
*/
const mimes =
{
	'bcmap' : 'application/octet-stream', // something something pdfjs
	'css'   : 'text/css',                 // cascading style sheet
	'eot'   : 'font/eot',                 // some font
	'gif'   : 'image/gif',                // image
	'html'  : 'text/html',                // hypertext
	'ico'   : 'image/x-icon',             // icon
	'jpg'   : 'image/jpeg',               // image
	'jpeg'  : 'image/jpeg',               // image
	'map'   : 'application/json',         // map
	'cjs'   : 'text/javascript',          // javascript
	'js'    : 'text/javascript',          // javascript
	'otf'   : 'font/otf',                 // some font
	'pdf'   : 'application/pdf',          // pdf file
	'pfb'   : 'application/x-font-type1', // some font
	'png'   : 'image/png',                // image
	'svg'   : 'image/svg+xml',            // some font or image
	'ttf'   : 'font/ttf',                 // some font
	'woff'  : 'application/font-woff',    // some font
	'woff2' : 'font/woff2',               // some font
};

/*
| Maps a file extension to a coding.
*/
def.static.coding =
	function( ext )
{
	const coding = codings[ ext ];
	if( !coding )
	{
		throw new Error( 'unknown file extension: .' + ext );
	}
	return coding;
};

/*
| Maps a file type to a mime.
*/
def.static.mime =
	function( ext )
{
	const mime = mimes[ ext ];
	if( !mime )
	{
		throw new Error( 'unknown file extension: ' + ext );
	}
	return mime;
};
