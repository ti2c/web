/*
| A browser bundle.
*/
def.attributes =
{
	// name of the combined resource (with hash)
	name: { type: [ 'undefined', 'string' ] },

	// resources of the combine
	combine: { type: [ 'undefined', 'Resource/Twig' ] },

	// scripts as resources
	// FIXME remove?
	scripts: { type: 'Resource/Twig' },

	// the ti2c walk
	walk: { type: 'ti2c:Walk' },
};

import fs         from 'node:fs/promises';
import terser     from 'terser';
import { Result } from './Result.js';

import { Self as GroupBoolean       } from '{group@boolean}';
import { Self as ResourceMemory     } from '{Resource/Memory}';
import { Self as ResourceTi2cClass  } from '{Resource/Ti2c/Class}';
import { Self as ResourceTi2cCode   } from '{Resource/Ti2c/Code}';
import { Self as ResourceTwig       } from '{Resource/Twig}';
import { Self as Sha1               } from '{Sha1}';
import { Self as Ti2cPackageManager } from '{ti2c:Package/Manager}';
import { Self as Ti2cPath           } from '{ti2c:Path}';
import { Self as Ti2cWeb            } from '{Self}';
import { Self as TypeTi2cClass      } from '{ti2c:Type/Ti2c/Class}';
import { Self as TypeTi2cGroup      } from '{ti2c:Type/Ti2c/Collection/Group}';
import { Self as TypeTi2cList       } from '{ti2c:Type/Ti2c/Collection/List}';
import { Self as TypeTi2cSet        } from '{ti2c:Type/Ti2c/Collection/Set}';
import { Self as TypeTi2cTwig       } from '{ti2c:Type/Ti2c/Collection/Twig}';

/*
| Builds the bundle.
|
| ~args: free strings
|
| 'beautify'         [boolean]       beautify minified code, default false
| 'entry'            [string]        ti2c import path of entry point
| 'extra'            [ResouceTwig]   extra resources
|                                    FIXME include into entries
| 'globalFlagsDevel' [group@boolean] globals to be defined in the bundle in devel mode
| 'globalFlagsIndex' [group@boolean] globals to be defined in the bundle in index mode
| 'log'              [function]      if defined calls this function for logging
|                                    FIXME use ti2c-log
| 'minify'           [boolean]       if true minifies the bundle, default true
| 'name'             [string]        name of the bundle
| 'offerDevel'       [boolean]       offers a devel.html, default true
| 'offerIndex'       [boolean]       offers a index.html, default true
| 'reportDir'        [ti2c:Path]     if defined writes reports here
*/
def.static.build =
	async function( ...args )
{
	let beautify = false;
	let log;
	let entry;
	let extra;
	let globalFlagsDevel;
	let globalFlagsIndex;
	let minify = true;
	let name;
	let offerDevel;
	let offerIndex;
	let reportDir;

	for( let a = 0, alen = args.length; a < alen; a+=2 )
	{
		const arg = args[ a ];
		const val = args[ a + 1 ];
		switch( arg )
		{
			case 'beautify':
/**/			if( CHECK && typeof( val ) !== 'boolean' ) throw new Error( );
				beautify = val;
				continue;

			case 'entry':
/**/			if( CHECK && typeof( val ) !== 'string' ) throw new Error( );
				entry = val;
				continue;

			case 'extra':
/**/			if( CHECK && val.ti2ctype !== ResourceTwig ) throw new Error( );
				extra = val;
				continue;

			case 'globalFlagsDevel':
/**/			if( CHECK && val.ti2ctype !== GroupBoolean ) throw new Error( );
				globalFlagsDevel = val;
				continue;

			case 'globalFlagsIndex':
/**/			if( CHECK && val.ti2ctype !== GroupBoolean ) throw new Error( );
				globalFlagsIndex = val;
				continue;

			case 'log':
				log = val;
				continue;

			case 'minify':
/**/			if( CHECK && typeof( val ) !== 'boolean' ) throw new Error( );
				minify = val;
				continue;

			case 'name':
/**/			if( CHECK && typeof( val ) !== 'string' ) throw new Error( );
				name = val;
				continue;

			case 'offerDevel':
/**/			if( CHECK && typeof( val ) !== 'boolean' ) throw new Error( );
				offerDevel = val;
				continue;

			case 'offerIndex':
/**/			if( CHECK && typeof( val ) !== 'boolean' ) throw new Error( );
				offerIndex = val;
				continue;

			case 'reportDir':
/**/			if( CHECK && val !== undefined && val.ti2ctype !== Ti2cPath ) throw new Error( );
				reportDir = val;
				continue;

			default: throw new Error( arg );
		}
	}

	if( log ) { log( 'building bundle' ); }

	const globalFlagsName = name + '/global-flags.js';

	// the bundled resources as twig
	let scripts = ResourceTwig.Empty;

	if( offerDevel || !minify )
	{
		scripts =
			scripts.create(
				'twig:add', globalFlagsName,
				Self._globalFlagsToResource( globalFlagsDevel )
			);
	}

	// the resource scripts to bundle
	if( extra )
	{
		scripts = scripts.appendTwig( extra );
	}

	scripts = scripts.appendTwig( Ti2cWeb.ti2cBaseResources );
	scripts = await scripts.prepare( );

	// prepares resources for the shell
	const walk = await Ti2cPackageManager.walk( entry );

	let resWalk = await Self._getResourcesByTi2cWalk( walk.types );
	resWalk = await resWalk.prepare( );
	scripts = scripts.appendTi2cWalk( resWalk );

	// updates the ti2c catalog init
	scripts = Self._updateTi2cCatalog( scripts );

	let bundleName;
	let brt;

	if( offerIndex )
	{
		let code, map;

		if( minify )
		{
			const result =
				await Self._buildMinify(
					log, name, scripts,
					globalFlagsName, globalFlagsIndex,
					beautify, reportDir
				);
			code = result.code;
			map = result.map;
		}
		else
		{
			const result = await Self._buildConcat( log, name, scripts );
			code = result.code;
			map = JSON.stringify( result.encodedMap( ) );
		}

		// calculates the hash for the bundle
		const hash = Sha1.calc( code );
		bundleName = name + '-' + hash + '.js';
		const sourceMapName = name + '-' + hash + '.map';

		brt =
			ResourceTwig.create(
				'twig:add', bundleName,
				ResourceMemory.JsDataLongSourceMapName(
					code + '\n//# sourceMappingURL=/' + sourceMapName,
					sourceMapName
				),
				'twig:add', sourceMapName,
				ResourceMemory.create(
					'age', 'long',
					'coding', 'utf-8',
					'data', map,
					'mime', 'application/json',
				)
			);

		if( log ) log( 'bundle:', bundleName );

		const bRes = brt.get( bundleName );
		const gzip = await bRes.gzip( );

		if( log )
		{
			log( 'uncompressed bundle size is ', bRes.data.length );
			log( '  compressed bundle size is ', gzip.length );
		}
	}

	return(
		Self.create(
			'name', bundleName,
			'combine', brt,
			'scripts', scripts,
			'walk', walk,
		)
	);
};

/*
| Builds an import map used in devel mode.
|
| ~scripts: resource twigs of all the scripts.
|
| ~returns: the import map as string.
*/
def.lazy.importMap =
	function( )
{
	// global imports
	// offers direct access to everything via {{ }} for ti2c-code to use
	const imports = [ ];
	const walk = this.walk;

	for( let type of walk.types )
	{
		imports.push( '"{{' + type.asString + '}}": "/' + type.asResourceString + '.js"' );
	}

	// scopes generated for actual imports
	const pkgNames = walk.pkgNames;
	const scopes = [ ];
	for( let scopeName of pkgNames )
	{
		const scope = scopes[ scopeName ] = [ ];
		const importSpecifiers = new Set( );

		for( let type of walk.types )
		{
			switch( type.ti2ctype )
			{
				case TypeTi2cClass:
				{
					const entry = ti2c.getEntry( type );
					for( let imp of entry.importSpecifiers )
					{
						importSpecifiers.add( imp );
					}
					break;
				}
			}
		}

		for( let ispec of importSpecifiers )
		{
			const type = Ti2cPackageManager.TypeFromSpecifier( ispec, scopeName );
			scope.push( '"{' + ispec + '}":"/' + type.asResourceString + '.js"' );
		}
	}

	const lines =
	[
		'<script type="importmap">',
		'{',
		'\t"imports": {',
	];

	// global direct imports
	for( let a = 0, alen = imports.length; a < alen; a++ )
	{
		const imp = imports[ a ];
		const comma = a + 1 < alen ? ',' : '';
		lines.push( '\t\t' + imp + comma );
	}
	lines.push( '\t},' );
	lines.push( '\t"scopes": {' );

	// all the scopes

	for( let a = 0, alen = pkgNames.length; a < alen; a++ )
	{
		const scopeName = pkgNames.get( a );
		const scope = scopes[ scopeName ];

		lines.push( '\t\t"/' + scopeName + '/": {' );

		for( let b = 0, blen = scope.length; b < blen; b++ )
		{
			const e = scope[ b ];
			const comma = b + 1 < blen ? ',' : '';
			lines.push( '\t\t\t' + e + comma );
		}

		const comma = a + 1 < alen ? ',' : '';
		lines.push( '\t\t}' + comma );
	}

	lines.push(
		'\t}',
		'}',
		'</script>',
	);
	return lines.join( '\n' );
};

/*
| Creates a minified script bundle.
|
| ~FIXME args.
*/
def.static._buildMinify =
	async function( log, name, scripts, globalFlagsName, globalFlagsIndex, beautify, reportDir )
{
	const concat = new Result( name + '.js' );
	for( let key of scripts.keys )
	{
		// minify uses the global flags directly
		if( key === globalFlagsName ) continue;

		const res = scripts.get( key );
		res.bundlize( concat );
	}

	const flags = globalFlagsIndex;
	const globals = { };
	for( let key of flags.keys )
	{
		globals[ key ] = flags.get( key );
	}

	const options =
	{
		compress:
		{
			ecma: 6,
			global_defs: globals,
		},
		module: true,
		output:
		{
			beautify: beautify,
		},
		sourceMap:
		{
			content: concat.decodedMap( ),
			filename: name + '.map',
		},
	};

	if( log ) log( 'minifying bundle' );

	if( reportDir )
	{
		await fs.writeFile( reportDir.f( name + '-code.js' ).asString, concat.code );
	}

	const result = await terser.minify( concat.code, options );

	if( result.error )
	{
		throw new Error( 'minify error: ' + result.error );
	}

	if( reportDir )
	{
		await fs.writeFile( reportDir.f( name + '.map' ).asString, result.map );
	}

	return result;
};

/*
| Creates a script bundle by simply concating all scripts.
|
| ~scripts:
*/
def.static._buildConcat =
	async function( log, name, scripts )
{
	if( log ) log( 'concating bundle' );

	const result = new Result( name + '.js' );

	for( let key of scripts.keys )
	{
		const res = scripts.get( key );
		res.bundlize( result );
	}

	return result;
};


/*
| Returns a twig of resources by walks the imports of a Ti2c class.
|
| ~entryName: root entry name to add the walk from
|             it must have been required already
*/
def.static._getResourcesByTi2cWalk =
	async function( typeWalk )
{
	let twig = { };
	let keys = [ ];

	for( let type of typeWalk )
	{
		const entry = ti2c.getEntry( type );
		const name = type.asResourceString + '.js';

		switch( type.ti2ctype )
		{
			case TypeTi2cClass:
				twig[ name ] =
					ResourceTi2cClass.create(
						'aPath', entry.aPathDef,
						'name', name,
						'type', type,
					);
				break;

			case TypeTi2cGroup:
			case TypeTi2cList:
			case TypeTi2cSet:
			case TypeTi2cTwig:
				twig[ name ] =
					ResourceTi2cCode.create(
						'aPath', entry.aPathCodeGen,
						'name', name,
						'type', type,
					);
				break;

			default: throw new Error;
		}

		keys.push( name );
	}

	return ResourceTwig.create( 'twig:init', twig, keys );
};

/*
| Returns the tree initialization code
| to be used in the browser.
|
| ~scripts: script resources
*/
def.static._getBrowserInitCode =
	function( scripts )
{
	const lines = [ 'const types = [' ];
	const types = ti2c.getAllEntryTypes( );
	for( let type of types )
	{
		//lines.push( '\t\'' + type.asString + '\':{Self:{},},' );
		lines.push( '\t\'' + type.asString + '\',' );
	}
	lines.push( '];' );

	lines.push(
		'const entries=globalThis.__ti2cEntries={};',
		'for(let t of types) {',
		'\tentries[ t ] = {',
		'\t\tSelf:{},',
		'\t\tdef:{proto:{},lazy:{},lazyFunc:{},static:{},staticLazy:{},staticLazyFunc:{}}',
		'\t};',
		'}',
	);

	lines.push( 'globalThis.ti2c_onload = function( ) {' );
	for( let key of scripts.keys )
	{
		const res = scripts.get( key );
		if( res.ti2ctype !== ResourceTi2cClass ) continue;

		const type = res.type;

		const entryEx = ti2c.getEntry( type ).entryExtend;
		const typeEx = entryEx?.type;

		lines.push(
			'\tti2c._prepare( '
			+ '__ti2cEntries[\'' + type.asString + '\']'
			+ (
				typeEx
				?  ',__ti2cEntries[\'' + typeEx.asString + '\']'
				: ''
			)
			+ ');'
		);
	}
	lines.push( '};', '' );

	return lines.join( '\n' );
};

/*
| Transforms the global flags into a resource.
| Used in devel mode.
|
| ~FIXME
*/
def.static._globalFlagsToResource =
	function( globals )
{
	let text = '';
	for( let key of globals.keys )
	{
		text += 'globalThis.' + key + ' = ' + globals.get( key ) + ';\n';
	}

	return ResourceMemory.JsData( text );
};

/*
| Adds the packages init resource.
*/
def.static._updateTi2cCatalog =
	function( scripts )
{
	const catalogName = 'ti2c/packages-init.js';
	let res = scripts.get( catalogName );
	res = res.create( 'data', Self._getBrowserInitCode( scripts ) );
	return scripts.set( catalogName, res );
};
