/*
| A ti2c class to be REST-served.
*/
def.extend = 'Resource/Base/Data';

def.attributes =
{
	// absolute path of ti2c class definition
	aPath: { type: 'ti2c:Path' },

	// ti2c type of the ti2c resource
	type: { type: 'ti2c:Type/Ti2c/Class' },

	// the name (aka URI location) of the resource
	name: { type: 'string' },

	// the ti2c-code resource for this ti2c class.
	tcResource: { type: [ 'undefined', 'Resource/Ti2c/Code' ] },

	// the original source code (without pre/postamble)
	_source: { type: [ 'undefined', 'string' ] },
};

import fs from 'node:fs/promises';

import { Self as Ast                } from '{ti2c:Ast}';
import { Self as AstBlock           } from '{ti2c:Ast/Block}';
import { Self as AstImport          } from '{ti2c:Ast/Import}';
import { Self as Formatter          } from '{ti2c:Format}';
import { Self as Parser             } from '{ti2c:Parser}';
import { Self as ResourceTi2cCode   } from '{Resource/Ti2c/Code}';
import { Self as Ti2cPackageManager } from '{ti2c:Package/Manager}';

const readOptions = Object.freeze( { encoding: 'utf8' } );

/*
| Is javascript.
*/
def.proto.coding = 'utf-8';
def.proto.mime = 'text/javascript';

/*
| Returns the code of the resource for the bundle.
|
| ~result: mutable(!) protean for the generation result with source map.
*/
def.proto.bundlize =
	function( result )
{
	console.log( 'parsing:', this.name );

	const source = this._source;
	const pAst = Parser.block( source );
	const aList = [ ];
	const pkgName = this.type.pkgName;

	// transforms imports.
	for( let s of pAst.statements )
	{
		if( s.ti2ctype !== AstImport )
		{
			aList.push( s );
		}
		else
		{
			let spec = s.moduleName;
			if( !spec.startsWith( '{' ) ) throw new Error( );
			if( !spec.endsWith( '}' ) ) throw new Error( );
			spec = spec.substring( 1, spec.length - 1 );

			const type = Ti2cPackageManager.TypeFromSpecifier( spec, pkgName );

			// FIXME make this more elegant
			let expr = Ast.var( '__ti2cEntries' );
			expr = Ast.member( expr, Ast.string( type.asString ) );
			expr = Ast.dot( expr, 'Self' );

			aList.push( Ast.const( s.aliasName, expr ) );
		}
	}

	const tAst = AstBlock.Array( aList );

	// terser currently needs this kind of capsule to be efficient
	result.addString( '(()=>{/*' + this.name + '*/\n' );
	result.addString( this._getPreamble( true, undefined, this.tcResource ) );
	Formatter.formatWithSourceMap( tAst, this.name, result );
	result.addString( '})();\n' );
};

/*
| Prepares this resource adds pre and postamble, creates the ti2c-code resource.
*/
def.proto.prepare =
	async function( )
{
	if( this.data )
	{
		return this;
	}

	const aPath = this.aPath;
	const type = this.type;

	let source;
	try
	{
		source = await fs.readFile( aPath.asString, readOptions );
	}
	catch( e )
	{
		console.log( 'Cannot read "' + aPath.asString + '"' );
		throw new Error( );
	}
	source += '';

	const entry = ti2c.getEntry( type );
	const aPathCodeGen = entry.aPathCodeGen;
	let tcResource =
		ResourceTi2cCode.create(
			'age', this.age,
			'aPath', aPathCodeGen,
			'type', type,
			'name', type.pkgName + '/ti2c-code/' + aPathCodeGen.last.key,
		);
	tcResource = await tcResource.prepare( );

	const preamble = this._getPreamble( false, entry, tcResource );
	const postamble = '\nexport { Self };\n';

	return(
		this.create(
			'data', preamble + source + postamble,
			'tcResource', tcResource,
			'_source', source,
		)
	);
};

/*
| Returns the preamble to be prepended
| to sources for browser mode.
|
| ~bundlize:    true if bundlizing / false for devel mode
| ~entry:       ti2c entry to create preamble for.
|               (only in devel mode, for release mode this is undefined)
| ~tcResource:  the generated code resource
*/
def.proto._getPreamble =
	function( bundlize, entry, tcResource )
{
	const extendType = entry?.entryExtend?.type;

	return(
		(
			entry
			?  'import "/' + tcResource.name + '";'
			: ''
		)
		+ (
			extendType
//			? 'import "/' + extendType.pkgName + '/' + extendType.entryName + '.js";'
			? 'import "{{' + extendType.asString + '}}";'
			: ''
		)
		+ 'const {Self,def}=__ti2cEntries[\'' + this.type.asString + '\'];'
	);
};
