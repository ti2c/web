/*
| Something to be REST-served.
*/
def.extend = 'Resource/Base/Data';

def.attributes =
{
	// "binary" or "utf-8"
	coding: { type: [ 'undefined', 'string' ] },

	// mime type
	mime: { type: [ 'undefined', 'string' ] },
};

import { Self as GroupString } from '{group@string}';

/*
| Does not have a file.
*/
def.proto.aPath = undefined;

/*
| Returns the code of the resource for the bundle.
*/
def.proto.bundlize =
	function( result )
{
	// terser currently needs this kind of capsule to be efficient
	result.addString(
		'(()=>{/*(memory)*/\n'
		+ this.data
		+ '})();\n'
	);
};

/*
| Shortcut.
*/
def.static.JsData =
	function( data )
{
	return(
		Self.create(
			'coding', 'utf-8',
			'data', data,
			'mime', 'text/javascript',
		)
	);
};

/*
| Shortcut.
*/
def.static.JsDataLong =
	function( data )
{
	return(
		Self.create(
			'age', 'long',
			'coding', 'utf-8',
			'data', data,
			'mime', 'text/javascript',
		)
	);
};

/*
| Shortcut.
*/
def.static.JsDataLongSourceMapName =
	function( data, sourceMapName )
{
	return(
		Self.create(
			'age', 'long',
			'coding', 'utf-8',
			'data', data,
			'header', GroupString.Table( { 'SourceMap': sourceMapName } ),
			'mime', 'text/javascript',
		)
	);
};

/*
| Adds this resource to a ti2c-web instance.
|
| ~tw:     ti2c-web instance to add to
| ~names:  the names to add the resource as
*/
def.proto._addToTi2cWeb =
	async function( tw, names )
{
	let resources = tw.resources;
	for( let name of names )
	{
		resources = resources.set( name, this );
	}
	return tw.create( 'resources', resources );
};
