/*
| Something to be REST-served.
*/
def.abstract = true;

/*
| Prepares the resource.
*/
def.proto.prepare =
	async function( )
{
	// defaults to do nothing
	return this;
};
