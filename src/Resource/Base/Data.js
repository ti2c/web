/*
| Something with fixed data to be REST-served.
*/
def.extend = 'Resource/Base/Self';
def.abstract = true;

def.attributes =
{
	// "none", "short" or "long"
	age: { type: 'string', defaultValue: '"none"' },

	// cached data
	data: { type: [ 'undefined', 'protean', 'string' ] },

	// extra headers to reply
	header: { type: [ 'undefined', 'group@string' ] },

	// cached gzip data
	_cache: { type: 'protean', defaultValue: '{ }' },
};

import zlib from 'node:zlib';
import util from 'node:util';

const gzip = util.promisify( zlib.gzip );

import { Self as GroupString } from '{group@string}';
import { Self as MaxAge      } from '{MaxAge}';

/*
| A header for same origin policy.
*/
def.staticLazy.headerSameOrigin =
	function( )
{
	return(
		GroupString.Table( {
			'Cross-Origin-Embedder-Policy': 'require-corp',
			'Cross-Origin-Opener-Policy': 'same-origin',
		} )
	);
};

/*
| Returns the gzipped data.
| Caches the result.
*/
def.proto.gzip =
	async function( )
{
	const cache = this._cache;
	let cg = cache.gzip;
	if( cg && cache.data === this.data ) return cg;
	cache.data = undefined;
	cg = cache.gzip = await gzip( this.data );
	cache.data = this.data;
	return cg;
};

/*
| Handles requests.
|
| ~request:  the http(s) request
| ~result:   the http(s) result
| ~ti2cWeb:  the ti2cWeb instance asking for handling
*/
def.proto._handle =
	async function( request, result, ti2cWeb )
{
	// this should not happen
	if( !this.data ) throw new Error( );

	let aenc = request.headers[ 'accept-encoding' ];

	const header =
	{
		'content-type': this.mime,
		'cache-control':
			ti2cWeb.requestCaching
			? MaxAge.map( this.age )
			: 'no-cache',
		'date': new Date( ).toUTCString( ),
	};

	const rHeader = this.header;
	if( rHeader )
	{
		for( let key of rHeader.keys )
		{
			header[ key ] = rHeader.get( key );
		}
	}

	if( aenc && aenc.indexOf( 'gzip' ) >= 0 )
	{
		// delivers compressed
		header[ 'content-encoding' ] = 'gzip';
		result.writeHead( 200, header );
		const gzip = await this.gzip( );
		result.end( gzip, 'binary' );
	}
	else
	{
		// delivers uncompressed
		result.writeHead( 200, header );
		result.end( this.data, this.coding );
	}
};
