/*
| Initalizes the ti2c environment in the browser.
*/
globalThis.ti2c = { };

/*
| Global pass flag for creators
*/
globalThis.pass = Object.freeze( { } );
