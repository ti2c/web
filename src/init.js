/*
| Initializes the ti2c-web package.
*/
await ti2c.register(
	'name',    'web',
	'meta',    import.meta,
	'source',  'src/',
	'relPath', 'init'
);
